# this module will be imported in the into your flowgraph

import os
import sys
import time
import numpy






def get_array(
        wtype='cw',
        delay=0,
        length=1,
        period=100,
        seed=0,
        amplitude=0.0,
        phase=0.0,
        duty_max=100.0,
        amplitude_max=0.35,
        ):

    duty  = float(length)
    duty /= float(period)
    duty *= 100.0

    if wtype=='cw':
    
        waveform = numpy.ones(length)
        waveform = numpy.append(waveform,numpy.zeros(period-length))
    
    if wtype=='barker':
    
        if length ==  2: waveform = [+1,-1]
        elif length ==  3: waveform = [+1,+1,-1]
        elif length ==  4: waveform = [+1,-1,+1,+1]
        elif length ==  5: waveform = [+1,+1,+1,-1,+1]
        elif length ==  7: waveform = [+1,+1,+1,-1,-1,+1,-1]
        elif length == 11: waveform = [+1,+1,+1,-1,-1,-1,+1,-1,-1,+1,-1]
        elif length == 13: waveform = [+1,+1,+1,+1,+1,-1,-1,+1,+1,-1,+1,-1,+1]
	else: waveform = numpy.zeros(length)

        waveform = numpy.append(waveform,numpy.zeros(period-length))


    if wtype == 'complementary':
    
        if length == 4:
        
            waveform_a = [+1,+1,+1,-1]
            waveform_b = [+1,+1,-1,+1]
        
        elif length == 8:
        
            waveform_a = [+1,+1,+1,-1,+1,+1,-1,+1],
            waveform_b = [+1,+1,+1,-1,-1,-1,+1,-1],
        
        elif length == 16:
        
            waveform_a = [+1,+1,+1,-1,+1,+1,-1,+1,+1,+1,+1,-1,-1,-1,+1,-1],
            waveform_b = [+1,+1,+1,-1,+1,+1,-1,+1,-1,-1,-1,+1,+1,+1,-1,+1],
        
        elif length == 32:
        
            waveform_a = [+1,+1,+1,-1,+1,+1,-1,+1,+1,+1,+1,-1,-1,-1,+1,-1,+1,+1,+1,-1,+1,+1,-1,+1,-1,-1,-1,+1,+1,+1,-1,+1],
            waveform_b = [+1,+1,+1,-1,+1,+1,-1,+1,+1,+1,+1,-1,-1,-1,+1,-1,-1,-1,-1,+1,-1,-1,+1,-1,+1,+1,+1,-1,-1,-1,+1,-1],

	else:
            
            waveform_a = numpy.zeros(length)
            waveform_b = numpy.zeros(length)

        waveform_a = numpy.append(waveform_a,numpy.zeros(period-length))
        waveform_b = numpy.append(waveform_b,numpy.zeros(period-length))
        waveform   = numpy.append(waveform_a,waveform_b)


    if wtype == 'pseudo':

        numpy.random.seed(seed)
        waveform = numpy.random.random(length)
        waveform = numpy.exp(2.0*numpy.pi*1.0j*waveform)
        waveform = numpy.angle(waveform)
        waveform = -1.0*numpy.sign(waveform)
        waveform = waveform.real
        waveform = numpy.append(waveform,numpy.zeros(period-length))

    waveform  = numpy.complex64(waveform)
    waveform  = numpy.roll(waveform,delay)
    if amplitude <= amplitude_max:
        waveform *= amplitude
    else:
        waveform *= 0.0
    if duty > duty_max:
        waveform *= 0.0
    waveform *= numpy.exp(1j*numpy.deg2rad(phase))
    waveform  = waveform.tolist()
    waveform  = tuple(waveform)

    return waveform
