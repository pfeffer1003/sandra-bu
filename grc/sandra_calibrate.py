#!/usr/bin/env python2
# -*- coding: utf-8 -*-
##################################################
# GNU Radio Python Flow Graph
# Title: Sandra Trx
# Generated: Fri Jun 12 10:31:33 2020
##################################################

if __name__ == '__main__':
    import ctypes
    import sys
    if sys.platform.startswith('linux'):
        try:
            x11 = ctypes.cdll.LoadLibrary('libX11.so')
            x11.XInitThreads()
        except:
            print "Warning: failed to XInitThreads()"

from PyQt5 import Qt
from PyQt5 import Qt, QtCore
from PyQt5.QtCore import QObject, pyqtSlot
from gnuradio import blocks
from gnuradio import eng_notation
from gnuradio import filter
from gnuradio import gr
from gnuradio import qtgui
from gnuradio import uhd
from gnuradio.eng_option import eng_option
from gnuradio.filter import firdes
from gnuradio.qtgui import Range, RangeWidget
from optparse import OptionParser
import numpy
import pmt
import sip
import sys
import time
import waveform  # embedded python module
from gnuradio import qtgui


class sandra_trx(gr.top_block, Qt.QWidget):

    def __init__(self, TX_CODE_PERIOD=200, TX_SYMBOL_RATE=200000):
        gr.top_block.__init__(self, "Sandra Trx")
        Qt.QWidget.__init__(self)
        self.setWindowTitle("Sandra Trx")
        qtgui.util.check_set_qss()
        try:
            self.setWindowIcon(Qt.QIcon.fromTheme('gnuradio-grc'))
        except:
            pass
        self.top_scroll_layout = Qt.QVBoxLayout()
        self.setLayout(self.top_scroll_layout)
        self.top_scroll = Qt.QScrollArea()
        self.top_scroll.setFrameStyle(Qt.QFrame.NoFrame)
        self.top_scroll_layout.addWidget(self.top_scroll)
        self.top_scroll.setWidgetResizable(True)
        self.top_widget = Qt.QWidget()
        self.top_scroll.setWidget(self.top_widget)
        self.top_layout = Qt.QVBoxLayout(self.top_widget)
        self.top_grid_layout = Qt.QGridLayout()
        self.top_layout.addLayout(self.top_grid_layout)

        self.settings = Qt.QSettings("GNU Radio", "sandra_trx")
        self.restoreGeometry(self.settings.value("geometry").toByteArray())

        ##################################################
        # Parameters
        ##################################################
        self.TX_CODE_PERIOD = TX_CODE_PERIOD
        self.TX_SYMBOL_RATE = TX_SYMBOL_RATE

        ##################################################
        # Variables
        ##################################################
        self.tx_waveform = tx_waveform = "cw"
        self.tx_seed = tx_seed = 0
        self.tx_phase = tx_phase = 0.0
        self.tx_max_duty = tx_max_duty = 100.0
        self.tx_magnitude_max = tx_magnitude_max = 1.0
        self.tx_magnitude = tx_magnitude = 0
        self.tx_length = tx_length = 1
        self.tx_delay = tx_delay = 0
        self.rx_phase_1 = rx_phase_1 = 0.0
        self.rx_phase_0 = rx_phase_0 = 0.0
        self.rx_delay = rx_delay = 0

        ##################################################
        # Blocks
        ##################################################
        Template error: #slurp
        #set $all_options = [$option0, $option1, $option2, $option3, $option4][:int($num_opts())]
        #set $all_labels = [$label0, $label1, $label2, $label3, $label4][:int($num_opts())]
        #if not $label()
        	#set $label = '"%s"'%$id
        #end if
        ########################################################################
        ## Create the options list
        ########################################################################
        #if int($num_opts())
        self._$(id)_options = (#slurp
        	#for $ch in $all_options
        $ch, #slurp
        	#end for
        )
        #else
        self._$(id)_options = $options
        #end if
        ########################################################################
        ## Create the labels list
        ########################################################################
        #if int($num_opts())
        self._$(id)_labels = (#slurp
        	#for i, $lbl in enumerate($all_labels)
        	#if $lbl()
        $lbl, #slurp
        	#else
        str(self._$(id)_options[$i]), #slurp
        	#end if
        	#end for
        )
        #elif $labels()
        self._$(id)_labels = $labels
        #else
        self._$(id)_labels = map(str, self._$(id)_options)
        #end if
        ########################################################################
        ## Create the combo box
        ########################################################################
        #if $widget() == 'combo_box'
        #set $win = 'self._%s_tool_bar'%$id
        $win = Qt.QToolBar(self)
        $(win).addWidget(Qt.QLabel($label+": "))
        self._$(id)_combo_box = Qt.QComboBox()
        $(win).addWidget(self._$(id)_combo_box)
        for label in self._$(id)_labels: self._$(id)_combo_box.addItem(label)
        self._$(id)_callback = lambda i: Qt.QMetaObject.invokeMethod(self._$(id)_combo_box, "setCurrentIndex", Qt.Q_ARG("int", self._$(id)_options.index(i)))
        self._$(id)_callback(self.$id)
        self._$(id)_combo_box.currentIndexChanged.connect(
        	lambda i: self.set_$(id)(self._$(id)_options[i]))
        #end if
        ########################################################################
        ## Create the radio buttons
        ########################################################################
        #if $widget() == 'radio_buttons'
        #set $win = 'self._%s_group_box'%$id
        $win = Qt.QGroupBox($label)
        self._$(id)_box = $(orient)()
        class variable_chooser_button_group(Qt.QButtonGroup):
            def __init__(self, parent=None):
                Qt.QButtonGroup.__init__(self, parent)
            @pyqtSlot(int)
            def updateButtonChecked(self, button_id):
                self.button(button_id).setChecked(True)
        self._$(id)_button_group = variable_chooser_button_group()
        $(win).setLayout(self._$(id)_box)
        for i, label in enumerate(self._$(id)_labels):
        	radio_button = Qt.QRadioButton(label)
        	self._$(id)_box.addWidget(radio_button)
        	self._$(id)_button_group.addButton(radio_button, i)
        self._$(id)_callback = lambda i: Qt.QMetaObject.invokeMethod(self._$(id)_button_group, "updateButtonChecked", Qt.Q_ARG("int", self._$(id)_options.index(i)))
        self._$(id)_callback(self.$id)
        self._$(id)_button_group.buttonClicked[int].connect(
        	lambda i: self.set_$(id)(self._$(id)_options[i]))
        #end if
        $(gui_hint() % $win)
            unsupported operand type(s) for %: 'GuiHint' and 'str'
        Template error: #set $win = 'self._%s_tool_bar'%$id
        $win = Qt.QToolBar(self)
        #if not $label()
        	#set $label = '"%s"'%$id
        #end if
        $(win).addWidget(Qt.QLabel($label+": "))
        self._$(id)_line_edit = Qt.QLineEdit(str(self.$id))
        self._$(id)_tool_bar.addWidget(self._$(id)_line_edit)
        self._$(id)_line_edit.returnPressed.connect(
        	lambda: self.set_$(id)($(type.conv)(str(self._$(id)_line_edit.text()))))
        $(gui_hint() % $win)
            unsupported operand type(s) for %: 'GuiHint' and 'str'
        Template error: #set $win = 'self._%s_win'%$id
        		#set $range = 'self._%s_range'%$id
        #if not $label()
        	#set $label = '"%s"'%$id
        #end if
        $(range) = Range($start, $stop, $step, $value, $min_len)
        $(win) = RangeWidget($range, self.set_$(id), $label, "$widget", $rangeType)
        $(gui_hint() % $win)
            unsupported operand type(s) for %: 'GuiHint' and 'str'
        Template error: #set $win = 'self._%s_tool_bar'%$id
        $win = Qt.QToolBar(self)
        #if not $label()
        	#set $label = '"%s"'%$id
        #end if
        $(win).addWidget(Qt.QLabel($label+": "))
        self._$(id)_line_edit = Qt.QLineEdit(str(self.$id))
        self._$(id)_tool_bar.addWidget(self._$(id)_line_edit)
        self._$(id)_line_edit.returnPressed.connect(
        	lambda: self.set_$(id)($(type.conv)(str(self._$(id)_line_edit.text()))))
        $(gui_hint() % $win)
            unsupported operand type(s) for %: 'GuiHint' and 'str'
        Template error: #set $win = 'self._%s_tool_bar'%$id
        $win = Qt.QToolBar(self)
        #if not $label()
        	#set $label = '"%s"'%$id
        #end if
        $(win).addWidget(Qt.QLabel($label+": "))
        self._$(id)_line_edit = Qt.QLineEdit(str(self.$id))
        self._$(id)_tool_bar.addWidget(self._$(id)_line_edit)
        self._$(id)_line_edit.returnPressed.connect(
        	lambda: self.set_$(id)($(type.conv)(str(self._$(id)_line_edit.text()))))
        $(gui_hint() % $win)
            unsupported operand type(s) for %: 'GuiHint' and 'str'
        Template error: #set $win = 'self._%s_win'%$id
        		#set $range = 'self._%s_range'%$id
        #if not $label()
        	#set $label = '"%s"'%$id
        #end if
        $(range) = Range($start, $stop, $step, $value, $min_len)
        $(win) = RangeWidget($range, self.set_$(id), $label, "$widget", $rangeType)
        $(gui_hint() % $win)
            unsupported operand type(s) for %: 'GuiHint' and 'str'
        Template error: #set $win = 'self._%s_tool_bar'%$id
        $win = Qt.QToolBar(self)
        #if not $label()
        	#set $label = '"%s"'%$id
        #end if
        $(win).addWidget(Qt.QLabel($label+": "))
        self._$(id)_line_edit = Qt.QLineEdit(str(self.$id))
        self._$(id)_tool_bar.addWidget(self._$(id)_line_edit)
        self._$(id)_line_edit.returnPressed.connect(
        	lambda: self.set_$(id)($(type.conv)(str(self._$(id)_line_edit.text()))))
        $(gui_hint() % $win)
            unsupported operand type(s) for %: 'GuiHint' and 'str'
        Template error: #set $win = 'self._%s_tool_bar'%$id
        $win = Qt.QToolBar(self)
        #if not $label()
        	#set $label = '"%s"'%$id
        #end if
        $(win).addWidget(Qt.QLabel($label+": "))
        self._$(id)_line_edit = Qt.QLineEdit(str(self.$id))
        self._$(id)_tool_bar.addWidget(self._$(id)_line_edit)
        self._$(id)_line_edit.returnPressed.connect(
        	lambda: self.set_$(id)($(type.conv)(str(self._$(id)_line_edit.text()))))
        $(gui_hint() % $win)
            unsupported operand type(s) for %: 'GuiHint' and 'str'
        Template error: #set $win = 'self._%s_win'%$id
        		#set $range = 'self._%s_range'%$id
        #if not $label()
        	#set $label = '"%s"'%$id
        #end if
        $(range) = Range($start, $stop, $step, $value, $min_len)
        $(win) = RangeWidget($range, self.set_$(id), $label, "$widget", $rangeType)
        $(gui_hint() % $win)
            unsupported operand type(s) for %: 'GuiHint' and 'str'
        Template error: #set $win = 'self._%s_win'%$id
        		#set $range = 'self._%s_range'%$id
        #if not $label()
        	#set $label = '"%s"'%$id
        #end if
        $(range) = Range($start, $stop, $step, $value, $min_len)
        $(win) = RangeWidget($range, self.set_$(id), $label, "$widget", $rangeType)
        $(gui_hint() % $win)
            unsupported operand type(s) for %: 'GuiHint' and 'str'
        Template error: #set $win = 'self._%s_win'%$id
        		#set $range = 'self._%s_range'%$id
        #if not $label()
        	#set $label = '"%s"'%$id
        #end if
        $(range) = Range($start, $stop, $step, $value, $min_len)
        $(win) = RangeWidget($range, self.set_$(id), $label, "$widget", $rangeType)
        $(gui_hint() % $win)
            unsupported operand type(s) for %: 'GuiHint' and 'str'
        self.uhd_usrp_source_0 = uhd.usrp_source(
        	",".join(("addr=192.168.10.2", "")),
        	uhd.stream_args(
        		cpu_format="fc32",
        		otw_format='sc16',
        		channels=range(2),
        	),
        )
        self.uhd_usrp_source_0.set_clock_source('external', 0)
        self.uhd_usrp_source_0.set_time_source('external', 0)
        self.uhd_usrp_source_0.set_subdev_spec("A:A A:B", 0)
        self.uhd_usrp_source_0.set_samp_rate(TX_SYMBOL_RATE)
        self.uhd_usrp_source_0.set_time_unknown_pps(uhd.time_spec())
        self.uhd_usrp_source_0.set_center_freq(32550000.0, 0)
        self.uhd_usrp_source_0.set_gain(0, 0)
        self.uhd_usrp_source_0.set_center_freq(32550000.0, 1)
        self.uhd_usrp_source_0.set_gain(0, 1)
        self.uhd_usrp_sink_0 = uhd.usrp_sink(
        	",".join(("addr=192.168.10.2", "")),
        	uhd.stream_args(
        		cpu_format="fc32",
        		otw_format='sc16',
        		channels=range(1),
        	),
        )
        self.uhd_usrp_sink_0.set_clock_source('external', 0)
        self.uhd_usrp_sink_0.set_time_source('external', 0)
        self.uhd_usrp_sink_0.set_subdev_spec("A:AB", 0)
        self.uhd_usrp_sink_0.set_samp_rate(TX_SYMBOL_RATE)
        self.uhd_usrp_sink_0.set_time_unknown_pps(uhd.time_spec())
        self.uhd_usrp_sink_0.set_center_freq(32550000.0, 0)
        self.uhd_usrp_sink_0.set_gain(0, 0)
        self.qtgui_vector_sink_f_0_2_0 = Template error: #set $win = 'self._%s_win'%$id
        qtgui.vector_sink_f(
            $vlen,
            $x_start,
            $x_step,
            $x_axis_label,
            $y_axis_label,
            $name,
            $nconnections \# Number of inputs
        )
        self.$(id).set_update_time($update_time)
        self.$(id).set_y_axis($ymin, $ymax)
        self.$(id).enable_autoscale($autoscale)
        self.$(id).enable_grid($grid)
        self.$(id).set_x_axis_units($x_units)
        self.$(id).set_y_axis_units($y_units)
        self.$(id).set_ref_level($ref_level)

        labels = [$label1, $label2, $label3, $label4, $label5,
                  $label6, $label7, $label8, $label9, $label10]
        widths = [$width1, $width2, $width3, $width4, $width5,
                  $width6, $width7, $width8, $width9, $width10]
        colors = [$color1, $color2, $color3, $color4, $color5,
                  $color6, $color7, $color8, $color9, $color10]
        alphas = [$alpha1, $alpha2, $alpha3, $alpha4, $alpha5,
                  $alpha6, $alpha7, $alpha8, $alpha9, $alpha10]
        for i in xrange($nconnections):
            if len(labels[i]) == 0:
                self.$(id).set_line_label(i, "Data {0}".format(i))
            else:
                self.$(id).set_line_label(i, labels[i])
            self.$(id).set_line_width(i, widths[i])
            self.$(id).set_line_color(i, colors[i])
            self.$(id).set_line_alpha(i, alphas[i])

        self._$(id)_win = sip.wrapinstance(self.$(id).pyqwidget(), Qt.QWidget)
        $(gui_hint() % $win)
            unsupported operand type(s) for %: 'GuiHint' and 'str'
        self.qtgui_vector_sink_f_0_2 = Template error: #set $win = 'self._%s_win'%$id
        qtgui.vector_sink_f(
            $vlen,
            $x_start,
            $x_step,
            $x_axis_label,
            $y_axis_label,
            $name,
            $nconnections \# Number of inputs
        )
        self.$(id).set_update_time($update_time)
        self.$(id).set_y_axis($ymin, $ymax)
        self.$(id).enable_autoscale($autoscale)
        self.$(id).enable_grid($grid)
        self.$(id).set_x_axis_units($x_units)
        self.$(id).set_y_axis_units($y_units)
        self.$(id).set_ref_level($ref_level)

        labels = [$label1, $label2, $label3, $label4, $label5,
                  $label6, $label7, $label8, $label9, $label10]
        widths = [$width1, $width2, $width3, $width4, $width5,
                  $width6, $width7, $width8, $width9, $width10]
        colors = [$color1, $color2, $color3, $color4, $color5,
                  $color6, $color7, $color8, $color9, $color10]
        alphas = [$alpha1, $alpha2, $alpha3, $alpha4, $alpha5,
                  $alpha6, $alpha7, $alpha8, $alpha9, $alpha10]
        for i in xrange($nconnections):
            if len(labels[i]) == 0:
                self.$(id).set_line_label(i, "Data {0}".format(i))
            else:
                self.$(id).set_line_label(i, labels[i])
            self.$(id).set_line_width(i, widths[i])
            self.$(id).set_line_color(i, colors[i])
            self.$(id).set_line_alpha(i, alphas[i])

        self._$(id)_win = sip.wrapinstance(self.$(id).pyqwidget(), Qt.QWidget)
        $(gui_hint() % $win)
            unsupported operand type(s) for %: 'GuiHint' and 'str'
        self.qtgui_vector_sink_f_0_0_0_0 = Template error: #set $win = 'self._%s_win'%$id
        qtgui.vector_sink_f(
            $vlen,
            $x_start,
            $x_step,
            $x_axis_label,
            $y_axis_label,
            $name,
            $nconnections \# Number of inputs
        )
        self.$(id).set_update_time($update_time)
        self.$(id).set_y_axis($ymin, $ymax)
        self.$(id).enable_autoscale($autoscale)
        self.$(id).enable_grid($grid)
        self.$(id).set_x_axis_units($x_units)
        self.$(id).set_y_axis_units($y_units)
        self.$(id).set_ref_level($ref_level)

        labels = [$label1, $label2, $label3, $label4, $label5,
                  $label6, $label7, $label8, $label9, $label10]
        widths = [$width1, $width2, $width3, $width4, $width5,
                  $width6, $width7, $width8, $width9, $width10]
        colors = [$color1, $color2, $color3, $color4, $color5,
                  $color6, $color7, $color8, $color9, $color10]
        alphas = [$alpha1, $alpha2, $alpha3, $alpha4, $alpha5,
                  $alpha6, $alpha7, $alpha8, $alpha9, $alpha10]
        for i in xrange($nconnections):
            if len(labels[i]) == 0:
                self.$(id).set_line_label(i, "Data {0}".format(i))
            else:
                self.$(id).set_line_label(i, labels[i])
            self.$(id).set_line_width(i, widths[i])
            self.$(id).set_line_color(i, colors[i])
            self.$(id).set_line_alpha(i, alphas[i])

        self._$(id)_win = sip.wrapinstance(self.$(id).pyqwidget(), Qt.QWidget)
        $(gui_hint() % $win)
            unsupported operand type(s) for %: 'GuiHint' and 'str'
        self.qtgui_vector_sink_f_0_0_0 = Template error: #set $win = 'self._%s_win'%$id
        qtgui.vector_sink_f(
            $vlen,
            $x_start,
            $x_step,
            $x_axis_label,
            $y_axis_label,
            $name,
            $nconnections \# Number of inputs
        )
        self.$(id).set_update_time($update_time)
        self.$(id).set_y_axis($ymin, $ymax)
        self.$(id).enable_autoscale($autoscale)
        self.$(id).enable_grid($grid)
        self.$(id).set_x_axis_units($x_units)
        self.$(id).set_y_axis_units($y_units)
        self.$(id).set_ref_level($ref_level)

        labels = [$label1, $label2, $label3, $label4, $label5,
                  $label6, $label7, $label8, $label9, $label10]
        widths = [$width1, $width2, $width3, $width4, $width5,
                  $width6, $width7, $width8, $width9, $width10]
        colors = [$color1, $color2, $color3, $color4, $color5,
                  $color6, $color7, $color8, $color9, $color10]
        alphas = [$alpha1, $alpha2, $alpha3, $alpha4, $alpha5,
                  $alpha6, $alpha7, $alpha8, $alpha9, $alpha10]
        for i in xrange($nconnections):
            if len(labels[i]) == 0:
                self.$(id).set_line_label(i, "Data {0}".format(i))
            else:
                self.$(id).set_line_label(i, labels[i])
            self.$(id).set_line_width(i, widths[i])
            self.$(id).set_line_color(i, colors[i])
            self.$(id).set_line_alpha(i, alphas[i])

        self._$(id)_win = sip.wrapinstance(self.$(id).pyqwidget(), Qt.QWidget)
        $(gui_hint() % $win)
            unsupported operand type(s) for %: 'GuiHint' and 'str'
        self.qtgui_vector_sink_f_0_0 = Template error: #set $win = 'self._%s_win'%$id
        qtgui.vector_sink_f(
            $vlen,
            $x_start,
            $x_step,
            $x_axis_label,
            $y_axis_label,
            $name,
            $nconnections \# Number of inputs
        )
        self.$(id).set_update_time($update_time)
        self.$(id).set_y_axis($ymin, $ymax)
        self.$(id).enable_autoscale($autoscale)
        self.$(id).enable_grid($grid)
        self.$(id).set_x_axis_units($x_units)
        self.$(id).set_y_axis_units($y_units)
        self.$(id).set_ref_level($ref_level)

        labels = [$label1, $label2, $label3, $label4, $label5,
                  $label6, $label7, $label8, $label9, $label10]
        widths = [$width1, $width2, $width3, $width4, $width5,
                  $width6, $width7, $width8, $width9, $width10]
        colors = [$color1, $color2, $color3, $color4, $color5,
                  $color6, $color7, $color8, $color9, $color10]
        alphas = [$alpha1, $alpha2, $alpha3, $alpha4, $alpha5,
                  $alpha6, $alpha7, $alpha8, $alpha9, $alpha10]
        for i in xrange($nconnections):
            if len(labels[i]) == 0:
                self.$(id).set_line_label(i, "Data {0}".format(i))
            else:
                self.$(id).set_line_label(i, labels[i])
            self.$(id).set_line_width(i, widths[i])
            self.$(id).set_line_color(i, colors[i])
            self.$(id).set_line_alpha(i, alphas[i])

        self._$(id)_win = sip.wrapinstance(self.$(id).pyqwidget(), Qt.QWidget)
        $(gui_hint() % $win)
            unsupported operand type(s) for %: 'GuiHint' and 'str'
        self.qtgui_vector_sink_f_0 = Template error: #set $win = 'self._%s_win'%$id
        qtgui.vector_sink_f(
            $vlen,
            $x_start,
            $x_step,
            $x_axis_label,
            $y_axis_label,
            $name,
            $nconnections \# Number of inputs
        )
        self.$(id).set_update_time($update_time)
        self.$(id).set_y_axis($ymin, $ymax)
        self.$(id).enable_autoscale($autoscale)
        self.$(id).enable_grid($grid)
        self.$(id).set_x_axis_units($x_units)
        self.$(id).set_y_axis_units($y_units)
        self.$(id).set_ref_level($ref_level)

        labels = [$label1, $label2, $label3, $label4, $label5,
                  $label6, $label7, $label8, $label9, $label10]
        widths = [$width1, $width2, $width3, $width4, $width5,
                  $width6, $width7, $width8, $width9, $width10]
        colors = [$color1, $color2, $color3, $color4, $color5,
                  $color6, $color7, $color8, $color9, $color10]
        alphas = [$alpha1, $alpha2, $alpha3, $alpha4, $alpha5,
                  $alpha6, $alpha7, $alpha8, $alpha9, $alpha10]
        for i in xrange($nconnections):
            if len(labels[i]) == 0:
                self.$(id).set_line_label(i, "Data {0}".format(i))
            else:
                self.$(id).set_line_label(i, labels[i])
            self.$(id).set_line_width(i, widths[i])
            self.$(id).set_line_color(i, colors[i])
            self.$(id).set_line_alpha(i, alphas[i])

        self._$(id)_win = sip.wrapinstance(self.$(id).pyqwidget(), Qt.QWidget)
        $(gui_hint() % $win)
            unsupported operand type(s) for %: 'GuiHint' and 'str'
        self.qtgui_const_sink_x_0_0_0 = Template error: #set $win = 'self._%s_win'%$id
        qtgui.$(type.fcn)(
        	$size, \#size
        	$name, \#name
        	#if $type.t == 'message' then 0 else $nconnections# \#number of inputs
        )
        self.$(id).set_update_time($update_time)
        self.$(id).set_y_axis($ymin, $ymax)
        self.$(id).set_x_axis($xmin, $xmax)
        self.$(id).set_trigger_mode($tr_mode, $tr_slope, $tr_level, $tr_chan, $tr_tag)
        self.$(id).enable_autoscale($autoscale)
        self.$(id).enable_grid($grid)
        self.$(id).enable_axis_labels($axislabels)

        if not $legend:
          self.$(id).disable_legend()

        labels = [$label1, $label2, $label3, $label4, $label5,
                  $label6, $label7, $label8, $label9, $label10]
        widths = [$width1, $width2, $width3, $width4, $width5,
                  $width6, $width7, $width8, $width9, $width10]
        colors = [$color1, $color2, $color3, $color4, $color5,
                  $color6, $color7, $color8, $color9, $color10]
        styles = [$style1, $style2, $style3, $style4, $style5,
                  $style6, $style7, $style8, $style9, $style10]
        markers = [$marker1, $marker2, $marker3, $marker4, $marker5,
                   $marker6, $marker7, $marker8, $marker9, $marker10]
        alphas = [$alpha1, $alpha2, $alpha3, $alpha4, $alpha5,
                  $alpha6, $alpha7, $alpha8, $alpha9, $alpha10]
        for i in xrange(#if $type.t == 'message' then 1 else $nconnections#):
            if len(labels[i]) == 0:
                self.$(id).set_line_label(i, "Data {0}".format(i))
            else:
                self.$(id).set_line_label(i, labels[i])
            self.$(id).set_line_width(i, widths[i])
            self.$(id).set_line_color(i, colors[i])
            self.$(id).set_line_style(i, styles[i])
            self.$(id).set_line_marker(i, markers[i])
            self.$(id).set_line_alpha(i, alphas[i])

        self._$(id)_win = sip.wrapinstance(self.$(id).pyqwidget(), Qt.QWidget)
        $(gui_hint() % $win)
            unsupported operand type(s) for %: 'GuiHint' and 'str'
        self.qtgui_const_sink_x_0_0 = Template error: #set $win = 'self._%s_win'%$id
        qtgui.$(type.fcn)(
        	$size, \#size
        	$name, \#name
        	#if $type.t == 'message' then 0 else $nconnections# \#number of inputs
        )
        self.$(id).set_update_time($update_time)
        self.$(id).set_y_axis($ymin, $ymax)
        self.$(id).set_x_axis($xmin, $xmax)
        self.$(id).set_trigger_mode($tr_mode, $tr_slope, $tr_level, $tr_chan, $tr_tag)
        self.$(id).enable_autoscale($autoscale)
        self.$(id).enable_grid($grid)
        self.$(id).enable_axis_labels($axislabels)

        if not $legend:
          self.$(id).disable_legend()

        labels = [$label1, $label2, $label3, $label4, $label5,
                  $label6, $label7, $label8, $label9, $label10]
        widths = [$width1, $width2, $width3, $width4, $width5,
                  $width6, $width7, $width8, $width9, $width10]
        colors = [$color1, $color2, $color3, $color4, $color5,
                  $color6, $color7, $color8, $color9, $color10]
        styles = [$style1, $style2, $style3, $style4, $style5,
                  $style6, $style7, $style8, $style9, $style10]
        markers = [$marker1, $marker2, $marker3, $marker4, $marker5,
                   $marker6, $marker7, $marker8, $marker9, $marker10]
        alphas = [$alpha1, $alpha2, $alpha3, $alpha4, $alpha5,
                  $alpha6, $alpha7, $alpha8, $alpha9, $alpha10]
        for i in xrange(#if $type.t == 'message' then 1 else $nconnections#):
            if len(labels[i]) == 0:
                self.$(id).set_line_label(i, "Data {0}".format(i))
            else:
                self.$(id).set_line_label(i, labels[i])
            self.$(id).set_line_width(i, widths[i])
            self.$(id).set_line_color(i, colors[i])
            self.$(id).set_line_style(i, styles[i])
            self.$(id).set_line_marker(i, markers[i])
            self.$(id).set_line_alpha(i, alphas[i])

        self._$(id)_win = sip.wrapinstance(self.$(id).pyqwidget(), Qt.QWidget)
        $(gui_hint() % $win)
            unsupported operand type(s) for %: 'GuiHint' and 'str'
        self.fft_filter_xxx_1_1_0_0 = filter.fft_filter_ccc(1, (waveform.get_array(wtype=tx_waveform,delay=rx_delay,length=tx_length,period=TX_CODE_PERIOD,seed=tx_seed,amplitude=1.0/numpy.sqrt(float(tx_length)),phase=rx_phase_1,duty_max=100.0,amplitude_max=1.0)[::-1]), 1)
        self.fft_filter_xxx_1_1_0_0.declare_sample_delay(0)
        self.fft_filter_xxx_1_1_0 = filter.fft_filter_ccc(1, (waveform.get_array(wtype=tx_waveform,delay=rx_delay,length=tx_length,period=TX_CODE_PERIOD,seed=tx_seed,amplitude=1.0/numpy.sqrt(float(tx_length)),phase=rx_phase_0,duty_max=100.0,amplitude_max=1.0)[::-1]), 1)
        self.fft_filter_xxx_1_1_0.declare_sample_delay(0)
        self.blocks_vector_to_stream_0 = blocks.vector_to_stream(gr.sizeof_gr_complex*1, TX_CODE_PERIOD)
        self.blocks_tags_strobe_0 = blocks.tags_strobe(gr.sizeof_gr_complex*1, pmt.intern("TEST"), TX_CODE_PERIOD, pmt.intern("strobe"))
        self.blocks_stream_to_vector_0_2_0 = blocks.stream_to_vector(gr.sizeof_float*1, TX_CODE_PERIOD)
        self.blocks_stream_to_vector_0_2 = blocks.stream_to_vector(gr.sizeof_float*1, TX_CODE_PERIOD)
        self.blocks_stream_to_vector_0_0_0_1 = blocks.stream_to_vector(gr.sizeof_float*1, TX_CODE_PERIOD)
        self.blocks_stream_to_vector_0_0_0_0_0 = blocks.stream_to_vector(gr.sizeof_float*1, TX_CODE_PERIOD)
        self.blocks_stream_to_vector_0_0_0_0 = blocks.stream_to_vector(gr.sizeof_float*1, TX_CODE_PERIOD)
        self.blocks_stream_to_vector_0_0_0 = blocks.stream_to_vector(gr.sizeof_float*1, TX_CODE_PERIOD)
        self.blocks_stream_to_vector_0_0 = blocks.stream_to_vector(gr.sizeof_float*1, TX_CODE_PERIOD)
        self.blocks_stream_to_vector_0 = blocks.stream_to_vector(gr.sizeof_float*1, TX_CODE_PERIOD)
        self.blocks_nlog10_ff_0_2_0 = blocks.nlog10_ff(10, 1, 0)
        self.blocks_nlog10_ff_0_2 = blocks.nlog10_ff(10, 1, 0)
        self.blocks_nlog10_ff_0_0 = blocks.nlog10_ff(10, 1, 0)
        self.blocks_nlog10_ff_0 = blocks.nlog10_ff(10, 1, 0)
        self.blocks_multiply_const_vxx_0 = blocks.multiply_const_vcc((numpy.exp(1j*numpy.deg2rad(-90.0)), ))
        self.blocks_complex_to_mag_squared_0_2_0 = blocks.complex_to_mag_squared(1)
        self.blocks_complex_to_mag_squared_0_2 = blocks.complex_to_mag_squared(1)
        self.blocks_complex_to_mag_squared_0_0 = blocks.complex_to_mag_squared(1)
        self.blocks_complex_to_mag_squared_0 = blocks.complex_to_mag_squared(1)
        self.blocks_complex_to_float_0_0 = blocks.complex_to_float(1)
        self.blocks_complex_to_float_0 = blocks.complex_to_float(1)
        self.blocks_add_xx_0_0_0_0 = blocks.add_vcc(1)
        self.blocks_add_xx_0_0_0 = blocks.add_vcc(1)
        self.blocks_add_xx_0_0 = blocks.add_vcc(1)
        self.blocks_add_xx_0 = blocks.add_vcc(1)
        self.SRC_tx_mag_pattern_1 = blocks.vector_source_c(waveform.get_array(wtype=tx_waveform,delay=tx_delay,length=tx_length,period=TX_CODE_PERIOD,seed=tx_seed,amplitude=(2**float(tx_magnitude))/(2**16.0),phase=tx_phase,duty_max=tx_max_duty,amplitude_max=tx_magnitude_max), True, TX_CODE_PERIOD, [])
        self.BLK_tx_symbol_delay_0_0_0_0 = blocks.delay(gr.sizeof_gr_complex*1, TX_CODE_PERIOD-10)
        self.BLK_tx_symbol_delay_0_0_0 = blocks.delay(gr.sizeof_gr_complex*1, TX_CODE_PERIOD-10)
        self.BLK_tx_symbol_delay_0_0 = blocks.delay(gr.sizeof_gr_complex*1, TX_CODE_PERIOD-13)

        ##################################################
        # Connections
        ##################################################
        self.connect((self.BLK_tx_symbol_delay_0_0, 0), (self.uhd_usrp_sink_0, 0))
        self.connect((self.BLK_tx_symbol_delay_0_0_0, 0), (self.blocks_add_xx_0, 1))
        self.connect((self.BLK_tx_symbol_delay_0_0_0, 0), (self.fft_filter_xxx_1_1_0, 0))
        self.connect((self.BLK_tx_symbol_delay_0_0_0_0, 0), (self.blocks_multiply_const_vxx_0, 0))
        self.connect((self.SRC_tx_mag_pattern_1, 0), (self.blocks_vector_to_stream_0, 0))
        self.connect((self.blocks_add_xx_0, 0), (self.blocks_complex_to_float_0_0, 0))
        self.connect((self.blocks_add_xx_0, 0), (self.blocks_complex_to_mag_squared_0, 0))
        self.connect((self.blocks_add_xx_0_0, 0), (self.blocks_complex_to_mag_squared_0_2, 0))
        self.connect((self.blocks_add_xx_0_0, 0), (self.qtgui_const_sink_x_0_0_0, 0))
        self.connect((self.blocks_add_xx_0_0_0, 0), (self.blocks_complex_to_float_0, 0))
        self.connect((self.blocks_add_xx_0_0_0, 0), (self.blocks_complex_to_mag_squared_0_0, 0))
        self.connect((self.blocks_add_xx_0_0_0_0, 0), (self.blocks_complex_to_mag_squared_0_2_0, 0))
        self.connect((self.blocks_add_xx_0_0_0_0, 0), (self.qtgui_const_sink_x_0_0, 0))
        self.connect((self.blocks_complex_to_float_0, 1), (self.blocks_stream_to_vector_0_0_0, 0))
        self.connect((self.blocks_complex_to_float_0, 0), (self.blocks_stream_to_vector_0_0_0_0, 0))
        self.connect((self.blocks_complex_to_float_0_0, 0), (self.blocks_stream_to_vector_0_0_0_0_0, 0))
        self.connect((self.blocks_complex_to_float_0_0, 1), (self.blocks_stream_to_vector_0_0_0_1, 0))
        self.connect((self.blocks_complex_to_mag_squared_0, 0), (self.blocks_nlog10_ff_0, 0))
        self.connect((self.blocks_complex_to_mag_squared_0_0, 0), (self.blocks_nlog10_ff_0_0, 0))
        self.connect((self.blocks_complex_to_mag_squared_0_2, 0), (self.blocks_nlog10_ff_0_2, 0))
        self.connect((self.blocks_complex_to_mag_squared_0_2_0, 0), (self.blocks_nlog10_ff_0_2_0, 0))
        self.connect((self.blocks_multiply_const_vxx_0, 0), (self.blocks_add_xx_0_0_0, 1))
        self.connect((self.blocks_multiply_const_vxx_0, 0), (self.fft_filter_xxx_1_1_0_0, 0))
        self.connect((self.blocks_nlog10_ff_0, 0), (self.blocks_stream_to_vector_0, 0))
        self.connect((self.blocks_nlog10_ff_0_0, 0), (self.blocks_stream_to_vector_0_0, 0))
        self.connect((self.blocks_nlog10_ff_0_2, 0), (self.blocks_stream_to_vector_0_2, 0))
        self.connect((self.blocks_nlog10_ff_0_2_0, 0), (self.blocks_stream_to_vector_0_2_0, 0))
        self.connect((self.blocks_stream_to_vector_0, 0), (self.qtgui_vector_sink_f_0, 0))
        self.connect((self.blocks_stream_to_vector_0_0, 0), (self.qtgui_vector_sink_f_0_0, 0))
        self.connect((self.blocks_stream_to_vector_0_0_0, 0), (self.qtgui_vector_sink_f_0_0_0, 1))
        self.connect((self.blocks_stream_to_vector_0_0_0_0, 0), (self.qtgui_vector_sink_f_0_0_0, 0))
        self.connect((self.blocks_stream_to_vector_0_0_0_0_0, 0), (self.qtgui_vector_sink_f_0_0_0_0, 0))
        self.connect((self.blocks_stream_to_vector_0_0_0_1, 0), (self.qtgui_vector_sink_f_0_0_0_0, 1))
        self.connect((self.blocks_stream_to_vector_0_2, 0), (self.qtgui_vector_sink_f_0_2, 0))
        self.connect((self.blocks_stream_to_vector_0_2_0, 0), (self.qtgui_vector_sink_f_0_2_0, 0))
        self.connect((self.blocks_tags_strobe_0, 0), (self.blocks_add_xx_0, 0))
        self.connect((self.blocks_tags_strobe_0, 0), (self.blocks_add_xx_0_0, 0))
        self.connect((self.blocks_tags_strobe_0, 0), (self.blocks_add_xx_0_0_0, 0))
        self.connect((self.blocks_tags_strobe_0, 0), (self.blocks_add_xx_0_0_0_0, 0))
        self.connect((self.blocks_vector_to_stream_0, 0), (self.BLK_tx_symbol_delay_0_0, 0))
        self.connect((self.fft_filter_xxx_1_1_0, 0), (self.blocks_add_xx_0_0, 1))
        self.connect((self.fft_filter_xxx_1_1_0_0, 0), (self.blocks_add_xx_0_0_0_0, 1))
        self.connect((self.uhd_usrp_source_0, 1), (self.BLK_tx_symbol_delay_0_0_0, 0))
        self.connect((self.uhd_usrp_source_0, 0), (self.BLK_tx_symbol_delay_0_0_0_0, 0))

    def closeEvent(self, event):
        self.settings = Qt.QSettings("GNU Radio", "sandra_trx")
        self.settings.setValue("geometry", self.saveGeometry())
        event.accept()

    def get_TX_CODE_PERIOD(self):
        return self.TX_CODE_PERIOD

    def set_TX_CODE_PERIOD(self, TX_CODE_PERIOD):
        self.TX_CODE_PERIOD = TX_CODE_PERIOD
        self.fft_filter_xxx_1_1_0_0.set_taps((waveform.get_array(wtype=self.tx_waveform,delay=self.rx_delay,length=self.tx_length,period=self.TX_CODE_PERIOD,seed=self.tx_seed,amplitude=1.0/numpy.sqrt(float(self.tx_length)),phase=self.rx_phase_1,duty_max=100.0,amplitude_max=1.0)[::-1]))
        self.fft_filter_xxx_1_1_0.set_taps((waveform.get_array(wtype=self.tx_waveform,delay=self.rx_delay,length=self.tx_length,period=self.TX_CODE_PERIOD,seed=self.tx_seed,amplitude=1.0/numpy.sqrt(float(self.tx_length)),phase=self.rx_phase_0,duty_max=100.0,amplitude_max=1.0)[::-1]))
        self.blocks_tags_strobe_0.set_nsamps(self.TX_CODE_PERIOD)
        self.SRC_tx_mag_pattern_1.set_data(waveform.get_array(wtype=self.tx_waveform,delay=self.tx_delay,length=self.tx_length,period=self.TX_CODE_PERIOD,seed=self.tx_seed,amplitude=(2**float(self.tx_magnitude))/(2**16.0),phase=self.tx_phase,duty_max=self.tx_max_duty,amplitude_max=self.tx_magnitude_max), [])
        self.BLK_tx_symbol_delay_0_0_0_0.set_dly(self.TX_CODE_PERIOD-10)
        self.BLK_tx_symbol_delay_0_0_0.set_dly(self.TX_CODE_PERIOD-10)
        self.BLK_tx_symbol_delay_0_0.set_dly(self.TX_CODE_PERIOD-13)

    def get_TX_SYMBOL_RATE(self):
        return self.TX_SYMBOL_RATE

    def set_TX_SYMBOL_RATE(self, TX_SYMBOL_RATE):
        self.TX_SYMBOL_RATE = TX_SYMBOL_RATE
        self.uhd_usrp_source_0.set_samp_rate(self.TX_SYMBOL_RATE)
        self.uhd_usrp_sink_0.set_samp_rate(self.TX_SYMBOL_RATE)
        self.qtgui_vector_sink_f_0_2_0.set_x_axis(0.0, 3e008/1000.0/float(self.TX_SYMBOL_RATE))
        self.qtgui_vector_sink_f_0_2.set_x_axis(0.0, 3e008/1000.0/float(self.TX_SYMBOL_RATE))
        self.qtgui_vector_sink_f_0_0_0_0.set_x_axis(0.0, 3e008/1000.0/float(self.TX_SYMBOL_RATE))
        self.qtgui_vector_sink_f_0_0_0.set_x_axis(0.0, 3e008/1000.0/float(self.TX_SYMBOL_RATE))
        self.qtgui_vector_sink_f_0_0.set_x_axis(0.0, 3e008/1000.0/float(self.TX_SYMBOL_RATE))
        self.qtgui_vector_sink_f_0.set_x_axis(0.0, 3e008/1000.0/float(self.TX_SYMBOL_RATE))

    def get_tx_waveform(self):
        return self.tx_waveform

    def set_tx_waveform(self, tx_waveform):
        self.tx_waveform = tx_waveform
        self._tx_waveform_callback(self.tx_waveform)
        self.fft_filter_xxx_1_1_0_0.set_taps((waveform.get_array(wtype=self.tx_waveform,delay=self.rx_delay,length=self.tx_length,period=self.TX_CODE_PERIOD,seed=self.tx_seed,amplitude=1.0/numpy.sqrt(float(self.tx_length)),phase=self.rx_phase_1,duty_max=100.0,amplitude_max=1.0)[::-1]))
        self.fft_filter_xxx_1_1_0.set_taps((waveform.get_array(wtype=self.tx_waveform,delay=self.rx_delay,length=self.tx_length,period=self.TX_CODE_PERIOD,seed=self.tx_seed,amplitude=1.0/numpy.sqrt(float(self.tx_length)),phase=self.rx_phase_0,duty_max=100.0,amplitude_max=1.0)[::-1]))
        self.SRC_tx_mag_pattern_1.set_data(waveform.get_array(wtype=self.tx_waveform,delay=self.tx_delay,length=self.tx_length,period=self.TX_CODE_PERIOD,seed=self.tx_seed,amplitude=(2**float(self.tx_magnitude))/(2**16.0),phase=self.tx_phase,duty_max=self.tx_max_duty,amplitude_max=self.tx_magnitude_max), [])

    def get_tx_seed(self):
        return self.tx_seed

    def set_tx_seed(self, tx_seed):
        self.tx_seed = tx_seed
        Qt.QMetaObject.invokeMethod(self._tx_seed_line_edit, "setText", Qt.Q_ARG("QString", str(self.tx_seed)))
        self.fft_filter_xxx_1_1_0_0.set_taps((waveform.get_array(wtype=self.tx_waveform,delay=self.rx_delay,length=self.tx_length,period=self.TX_CODE_PERIOD,seed=self.tx_seed,amplitude=1.0/numpy.sqrt(float(self.tx_length)),phase=self.rx_phase_1,duty_max=100.0,amplitude_max=1.0)[::-1]))
        self.fft_filter_xxx_1_1_0.set_taps((waveform.get_array(wtype=self.tx_waveform,delay=self.rx_delay,length=self.tx_length,period=self.TX_CODE_PERIOD,seed=self.tx_seed,amplitude=1.0/numpy.sqrt(float(self.tx_length)),phase=self.rx_phase_0,duty_max=100.0,amplitude_max=1.0)[::-1]))
        self.SRC_tx_mag_pattern_1.set_data(waveform.get_array(wtype=self.tx_waveform,delay=self.tx_delay,length=self.tx_length,period=self.TX_CODE_PERIOD,seed=self.tx_seed,amplitude=(2**float(self.tx_magnitude))/(2**16.0),phase=self.tx_phase,duty_max=self.tx_max_duty,amplitude_max=self.tx_magnitude_max), [])

    def get_tx_phase(self):
        return self.tx_phase

    def set_tx_phase(self, tx_phase):
        self.tx_phase = tx_phase
        self.SRC_tx_mag_pattern_1.set_data(waveform.get_array(wtype=self.tx_waveform,delay=self.tx_delay,length=self.tx_length,period=self.TX_CODE_PERIOD,seed=self.tx_seed,amplitude=(2**float(self.tx_magnitude))/(2**16.0),phase=self.tx_phase,duty_max=self.tx_max_duty,amplitude_max=self.tx_magnitude_max), [])

    def get_tx_max_duty(self):
        return self.tx_max_duty

    def set_tx_max_duty(self, tx_max_duty):
        self.tx_max_duty = tx_max_duty
        Qt.QMetaObject.invokeMethod(self._tx_max_duty_line_edit, "setText", Qt.Q_ARG("QString", eng_notation.num_to_str(self.tx_max_duty)))
        self.SRC_tx_mag_pattern_1.set_data(waveform.get_array(wtype=self.tx_waveform,delay=self.tx_delay,length=self.tx_length,period=self.TX_CODE_PERIOD,seed=self.tx_seed,amplitude=(2**float(self.tx_magnitude))/(2**16.0),phase=self.tx_phase,duty_max=self.tx_max_duty,amplitude_max=self.tx_magnitude_max), [])

    def get_tx_magnitude_max(self):
        return self.tx_magnitude_max

    def set_tx_magnitude_max(self, tx_magnitude_max):
        self.tx_magnitude_max = tx_magnitude_max
        Qt.QMetaObject.invokeMethod(self._tx_magnitude_max_line_edit, "setText", Qt.Q_ARG("QString", eng_notation.num_to_str(self.tx_magnitude_max)))
        self.SRC_tx_mag_pattern_1.set_data(waveform.get_array(wtype=self.tx_waveform,delay=self.tx_delay,length=self.tx_length,period=self.TX_CODE_PERIOD,seed=self.tx_seed,amplitude=(2**float(self.tx_magnitude))/(2**16.0),phase=self.tx_phase,duty_max=self.tx_max_duty,amplitude_max=self.tx_magnitude_max), [])

    def get_tx_magnitude(self):
        return self.tx_magnitude

    def set_tx_magnitude(self, tx_magnitude):
        self.tx_magnitude = tx_magnitude
        self.SRC_tx_mag_pattern_1.set_data(waveform.get_array(wtype=self.tx_waveform,delay=self.tx_delay,length=self.tx_length,period=self.TX_CODE_PERIOD,seed=self.tx_seed,amplitude=(2**float(self.tx_magnitude))/(2**16.0),phase=self.tx_phase,duty_max=self.tx_max_duty,amplitude_max=self.tx_magnitude_max), [])

    def get_tx_length(self):
        return self.tx_length

    def set_tx_length(self, tx_length):
        self.tx_length = tx_length
        Qt.QMetaObject.invokeMethod(self._tx_length_line_edit, "setText", Qt.Q_ARG("QString", str(self.tx_length)))
        self.fft_filter_xxx_1_1_0_0.set_taps((waveform.get_array(wtype=self.tx_waveform,delay=self.rx_delay,length=self.tx_length,period=self.TX_CODE_PERIOD,seed=self.tx_seed,amplitude=1.0/numpy.sqrt(float(self.tx_length)),phase=self.rx_phase_1,duty_max=100.0,amplitude_max=1.0)[::-1]))
        self.fft_filter_xxx_1_1_0.set_taps((waveform.get_array(wtype=self.tx_waveform,delay=self.rx_delay,length=self.tx_length,period=self.TX_CODE_PERIOD,seed=self.tx_seed,amplitude=1.0/numpy.sqrt(float(self.tx_length)),phase=self.rx_phase_0,duty_max=100.0,amplitude_max=1.0)[::-1]))
        self.SRC_tx_mag_pattern_1.set_data(waveform.get_array(wtype=self.tx_waveform,delay=self.tx_delay,length=self.tx_length,period=self.TX_CODE_PERIOD,seed=self.tx_seed,amplitude=(2**float(self.tx_magnitude))/(2**16.0),phase=self.tx_phase,duty_max=self.tx_max_duty,amplitude_max=self.tx_magnitude_max), [])

    def get_tx_delay(self):
        return self.tx_delay

    def set_tx_delay(self, tx_delay):
        self.tx_delay = tx_delay
        Qt.QMetaObject.invokeMethod(self._tx_delay_line_edit, "setText", Qt.Q_ARG("QString", str(self.tx_delay)))
        self.SRC_tx_mag_pattern_1.set_data(waveform.get_array(wtype=self.tx_waveform,delay=self.tx_delay,length=self.tx_length,period=self.TX_CODE_PERIOD,seed=self.tx_seed,amplitude=(2**float(self.tx_magnitude))/(2**16.0),phase=self.tx_phase,duty_max=self.tx_max_duty,amplitude_max=self.tx_magnitude_max), [])

    def get_rx_phase_1(self):
        return self.rx_phase_1

    def set_rx_phase_1(self, rx_phase_1):
        self.rx_phase_1 = rx_phase_1
        self.fft_filter_xxx_1_1_0_0.set_taps((waveform.get_array(wtype=self.tx_waveform,delay=self.rx_delay,length=self.tx_length,period=self.TX_CODE_PERIOD,seed=self.tx_seed,amplitude=1.0/numpy.sqrt(float(self.tx_length)),phase=self.rx_phase_1,duty_max=100.0,amplitude_max=1.0)[::-1]))

    def get_rx_phase_0(self):
        return self.rx_phase_0

    def set_rx_phase_0(self, rx_phase_0):
        self.rx_phase_0 = rx_phase_0
        self.fft_filter_xxx_1_1_0.set_taps((waveform.get_array(wtype=self.tx_waveform,delay=self.rx_delay,length=self.tx_length,period=self.TX_CODE_PERIOD,seed=self.tx_seed,amplitude=1.0/numpy.sqrt(float(self.tx_length)),phase=self.rx_phase_0,duty_max=100.0,amplitude_max=1.0)[::-1]))

    def get_rx_delay(self):
        return self.rx_delay

    def set_rx_delay(self, rx_delay):
        self.rx_delay = rx_delay
        self.fft_filter_xxx_1_1_0_0.set_taps((waveform.get_array(wtype=self.tx_waveform,delay=self.rx_delay,length=self.tx_length,period=self.TX_CODE_PERIOD,seed=self.tx_seed,amplitude=1.0/numpy.sqrt(float(self.tx_length)),phase=self.rx_phase_1,duty_max=100.0,amplitude_max=1.0)[::-1]))
        self.fft_filter_xxx_1_1_0.set_taps((waveform.get_array(wtype=self.tx_waveform,delay=self.rx_delay,length=self.tx_length,period=self.TX_CODE_PERIOD,seed=self.tx_seed,amplitude=1.0/numpy.sqrt(float(self.tx_length)),phase=self.rx_phase_0,duty_max=100.0,amplitude_max=1.0)[::-1]))


def argument_parser():
    parser = OptionParser(usage="%prog: [options]", option_class=eng_option)
    parser.add_option(
        "", "--TX-CODE-PERIOD", dest="TX_CODE_PERIOD", type="intx", default=200,
        help="Set Code Period [default=%default]")
    parser.add_option(
        "", "--TX-SYMBOL-RATE", dest="TX_SYMBOL_RATE", type="intx", default=200000,
        help="Set Symbol Rate [default=%default]")
    return parser


def main(top_block_cls=sandra_trx, options=None):
    if options is None:
        options, _ = argument_parser().parse_args()

    from distutils.version import StrictVersion
    if StrictVersion(Qt.qVersion()) >= StrictVersion("4.5.0"):
        style = gr.prefs().get_string('qtgui', 'style', 'raster')
        Qt.QApplication.setGraphicsSystem(style)
    qapp = Qt.QApplication(sys.argv)

    tb = top_block_cls(TX_CODE_PERIOD=options.TX_CODE_PERIOD, TX_SYMBOL_RATE=options.TX_SYMBOL_RATE)
    tb.start()
    tb.show()

    def quitting():
        tb.stop()
        tb.wait()
    qapp.connect(qapp, Qt.SIGNAL("aboutToQuit()"), quitting)
    qapp.exec_()


if __name__ == '__main__':
    main()
