#!/usr/bin/env python



###############################################################################
# DESCRIPTION HEADER                                                          #
###############################################################################

# AUTHOR        :       M.Eng. Nico Pfeffer
# E-MAIL        :       pfeffer@iap-kborn.de
# DEPARTMENT    :       Radar-Remote-Sensing
# GROUP         :       Radio-Science
# COUNTRY       :       Germany
# STATE         :       Mecklenburg-Vorpommern
# LOCATION      :       Kuehlungsborn
# POSTAL        :       18225
# ADDRESS       :       Schlossstr. 6



###############################################################################
# IMPORTS                                                                     #
###############################################################################

import setuptools


setuptools.setup(
    name='sandra',
    version='1.0',
    description='SanDRA - Software Defined Radar in Atmopsheric Research',
    classifiers=[
        'Development Status :: 1 - Planning',
        'Intended Audience :: Science/Research',
        'Natural Language :: English',
        'Operating System :: POSIX :: Linux',
        'License :: OSI Approved :: MIT License',
        'Programming Language :: Python :: 2.7',
        'Topic :: Scientific/Engineering :: Atmospheric Science',
    ],
    #install_requires=[
    #    'python-intervals==1.8.0',
    #    'validation',
    #    'docopt',
    #    'django==1.7.4',
    #    'pymongo==3.7.2',
    #    'config==0.3.9',
    #    'mkl',
    #    'ipp',
    #    'mkl_fft',
    #    'mkl_random',
    #    'intel-numpy==1.15.1',
    #    'intel-scipy==1.1.0',
    #    'matplotlib==2.2.3',
    #],
    keywords='radar dsp sdr gnuradio tx rx mimo',
    author='M.Eng. Nico Pfeffer',
    author_email='pfeffer@iap-kborn.de',
    license='MIT',
    test_suite='nose.collector',
    tests_require=['nose'],
    packages=setuptools.find_packages(),
    zip_safe=False,
)
