import os
import config
import sandra
import xmlrpclib



class _Client(xmlrpclib.Server):

    def __init__(self,alias):
        self._alias = alias
        self.__init__log()
        self.__init__cfg()
        self.__init__rpc()

    def __init__log(self):
        pass

    def __init__cfg(self):
        cfg_file = "%s/.sandra/cfg/%s/%s.cfg"%(os.getenv('HOME'),self._mode,self._alias)
        sandra.tools.verify.instance(cfg_file,str)
        sandra.tools.verify.file(cfg_file)
        self.__cfg = config.Config(cfg_file)

    def __init__rpc(self):
        port = self.__cfg['Server']['Port']
        sandra.tools.verify.instance(port,int)
        xmlrpclib.Server.__init__(self,'http://localhost:%s'%(port))



class rx(_Client):
    _mode = "rx"
    def __init__(self,alias):
        _Client.__init__(self,alias)

class tx(_Client):
    _mode = "tx"
    def __init__(self,alias):
        _Client.__init__(self,alias)

class trx(_Client):
    _mode = "trx"
    def __init__(self,alias):
        _Client.__init__(self,alias)