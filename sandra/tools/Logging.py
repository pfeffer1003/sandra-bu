import os
import sys
import colorlog
import logging
import logging.handlers



class Logging(object):

    # ----------------------------------------------------------------------- #

    def __init__(self):
        pass

    # ----------------------------------------------------------------------- #

    @staticmethod
    def info(mode,alias=None):
        """
        """
        if mode == 'power':
            os.system("tail -n 100 %s/.sandra/log/power.log"%(os.getenv("HOME")))
        else:
            os.system("tail -n 100 %s/.sandra/log/%s/%s.log"%(os.getenv("HOME"),mode,alias))

    # ----------------------------------------------------------------------- #

    @staticmethod
    def file(filename):
        """
        """
        formatter  = ''
        formatter += '%(asctime)-15s '
        formatter += '%(levelname)-8s '
        formatter += '%(message)s'
        formatter  = logging.Formatter(formatter)

        handler = logging.handlers.TimedRotatingFileHandler(
            filename=filename,
            when="midnight",
            interval=1,
            backupCount=7,
            encoding=None,
            delay=False,
            utc=True)
        handler.setFormatter(formatter)

        log = logging.getLogger()
        log.setLevel(logging.DEBUG)
        log.addHandler(handler)
        
        return log

    # ----------------------------------------------------------------------- #

    @staticmethod
    def terminal():
        """
        """
        formatter  = "  %(log_color)s%(levelname)-8s%(reset)s | "
        formatter += "%(log_color)s%(message)s%(reset)s"
        formatter  = colorlog.ColoredFormatter(
            formatter,
            log_colors={
                'DEBUG'    : 'blue',
                'INFO'     : 'blue',
                'WARNING'  : 'yellow',
                'ERROR'    : 'red',
                'CRITICAL' : 'bold_red',
            }
        )

        handler = logging.StreamHandler()
        handler.setLevel(logging.DEBUG)
        handler.setFormatter(formatter)  

        log = logging.getLogger()
        log.setLevel(logging.DEBUG)
        log.addHandler(handler)
        
        return log

    # ----------------------------------------------------------------------- #