import os
import sys



def file(value):
    if not os.path.isfile(value):
        raise IOError()

def instance(value,dtype):
    if not isinstance(value,dtype):
        raise TypeError()