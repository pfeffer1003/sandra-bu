import os
import sys
import time
import config
import sandra
import threading
import SimpleXMLRPCServer



class _Server(object):

    def __init__(self,alias):
        self._alias = alias
        self.__init__log()
        self.__init__cfg()
        self.__init__rpc()

    def __init__log(self):
        pass

    def __init__cfg(self):
        cfg_file = "%s/.sandra/cfg/%s/%s.cfg"%(os.getenv('HOME'),self._mode,self._alias)
        sandra.tools.verify.instance(cfg_file,str)
        sandra.tools.verify.file(cfg_file)
        self.__cfg = config.Config(cfg_file)

    def __init__rpc(self):
        port = self.__cfg['Server']['Port']
        sandra.tools.verify.instance(port,int)
        server = SimpleXMLRPCServer.SimpleXMLRPCServer(
            addr=("127.0.0.1",port),
            allow_none=True,
            logRequests=False)
        server.register_instance(self)
        thread = threading.Thread(target=server.serve_forever)
        thread.daemon = True
        thread.start()

    def get_regards(self):
        return "Hello %s"%self._regards

    def run(self):
        """
        """
        while True:
            time.sleep(1.0)



class rx(_Server):

    _mode = "rx"
    _regards = "Receiver"
    
    def __init__(self,alias):
        _Server.__init__(self,alias)

class tx(_Server):

    _mode = "tx"
    _regards = "Transmitter"
    
    def __init__(self,alias):
        _Server.__init__(self,alias)

class trx(_Server):

    _mode = "trx"
    _regards = "Transceiver"
    
    def __init__(self,alias):
        _Server.__init__(self,alias)