import os
import subprocess



def show():
    os.system('systemctl --user list-units %s %s %s %s --all --no-pager'%(
            'sandra-power*',
            'sandra-tx@*',
            'sandra-rx@*',
            'sandra-trx@*',
        )
    )



class _Daemon(object):

    @classmethod
    def show(cls,alias=None):
        if alias is None:
            os.system('systemctl --user list-units sandra-%s@* --all --no-pager'%(cls._mode))
        else:
            os.system('systemctl --user list-units sandra-%s@%s.service --no-pager'%(cls._mode,alias))

    @classmethod
    def stop(cls,alias):
        os.system('systemctl --user stop sandra-%s@%s.service'%(cls._mode,alias))

    @classmethod
    def start(cls,alias):
        os.system('systemctl --user start sandra-%s@%s.service'%(cls._mode,alias))

    @classmethod
    def status(cls,alias):
        os.system('systemctl --user status sandra-%s@%s.service --no-pager'%(cls._mode,alias))

    @classmethod
    def enable(cls,alias):
        os.system('systemctl --user enable sandra-%s@%s.service'%(cls._mode,alias))

    @classmethod
    def disable(cls,alias):
        os.system('systemctl --user disable sandra-%s@%s.service'%(cls._mode,alias))

    @classmethod
    def restart(cls,alias):
        os.system('systemctl --user restart sandra-%s@%s.service'%(cls._mode,alias))

    @classmethod
    def reload(cls):
        os.system('systemctl --user daemon-reload')

    @classmethod
    def get_enabled(cls,alias):
        try:
            enabled = subprocess.check_output('systemctl --user is-enabled sandra-%s@%s.service'%(cls._mode,alias),shell=True)
            enabled = enabled.replace('\n','')
        except subprocess.CalledProcessError:
            enabled = 'no-response'
        return enabled

    @classmethod
    def get_active(cls,alias):
        try:
            active = subprocess.check_output('systemctl --user is-active sandra-%s@%s.service'%(cls._mode,alias),shell=True)
            active = active.replace('\n','')
        except subprocess.CalledProcessError:
            active = 'no-response'
        return active

    @classmethod
    def get_failed(cls,alias):
        try:
            failed = subprocess.check_output('systemctl --user is-failed sandra-%s@%s.service'%(cls._mode,alias),shell=True)
            failed = failed.replace('\n','')
        except subprocess.CalledProcessError:
            failed = 'no-response'
        return failed

    @classmethod
    def get_status(cls,alias):
        status = {}
        status.update({'enabled':cls.get_enabled(alias)})
        status.update({'active':cls.get_active(alias)})
        status.update({'failed':cls.get_failed(alias)})
        return status


class rx(_Daemon): _mode = 'rx'
class tx(_Daemon): _mode = 'tx'
class trx(_Daemon): _mode = 'trx'
