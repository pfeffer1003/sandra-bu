
# coding: utf-8

# In[106]:




# self.__threshold
#   get_threshold()
#   set_threshold() -> soll None akzeptieren oder soll dtype haben und innerhalb von lower upper





class Quantity(object):

    # descriptors
    __DTYPES = [int, float, str, bool, long, complex]
    __ITYPES = ['closed','open']
    
    def __init__(self, dtype, lower, upper, name, itype, unit="",threshold=None):
        
        # parameter
        self.__init__dtype(dtype)
        self.__init__unit(unit)
        self.__init__name(name)

        # variables
        self.set_itype(itype)
        self.set_lower(lower)
        self.set_upper(upper)


    def __init__dtype(self,dtype):
        if not dtype in self.__DTYPES:
            raise TypeError("Attribute <dtype> not in %s." % self.__DTYPES)
        self.__dtype = dtype
    
    def __init__unit(self,unit):
        if not isinstance(unit, str):
            raise TypeError("Attribute <unit> must be of <type 'str'>.")
        self.__unit = unit
    
    def __init__name(self, name):
        if not isinstance(name, str):
            raise TypeError("Attribute <name> must be of <type 'str'>.")
        self.__name = name

    def get_dtype(self):
        """
        """
        return self.__dtype
    
    def get_unit(self):
        """
        """
        return self.__unit
    
    def get_name(self):
        """
        """
        return self.__name
    
    def get_lower(self):
        """
        """
        return self.__lower

    def set_lower(self, lower):
        """
        """
        if not isinstance(lower, self.__dtype):
            raise TypeError("Argument <lower> must be of %s." % self.__dtype)
        self.__lower = lower

        
    def get_upper(self):
        """
        """
        return self.__upper
        
    def set_upper(self, upper):
        """
        """
        if not isinstance(upper, self.__dtype):
            raise TypeError("Argument <upper> must be of %s." % self.__dtype)
        self.__upper = upper
    
    
    def get_itype(self):
        """
        """
        return self.__itype
    
    def set_itype(self, itype):
        """
        """
        if not itype in self.__ITYPES:
            raise TypeError("Attribute <itype> not in %s." % self.__ITYPES)
        self.__itype = itype
        
        
    def verify(self, value):
        """
        """
        # proof if value has dtype
        if not isinstance(value, self.__dtype):
            raise TypeError("Argument <value> must be of %s." % self.__dtype)
        
        if self.__itype == 'closed':
            if not value >= self.__lower:
                raise ValueError("Argument <value> must be >= %s." % self.__lower)
            if not value <= self.__upper:
                raise ValueError("Argument <value> must be <= %s." % self.__upper)
        
        else:
            if not value > self.__lower:
                raise ValueError("Argument <value> must be > %s." % self.__lower)

            if not value < self.__upper:
                raise ValueError("Argument <value> must be < %s." % self.__upper)

        return value


quantity_delay = Quantity(dtype=int, lower=0, upper=1000, name='name', itype='closed')
# quantity_delay.set_lower(0.0)
# quantity_delay.verify(1000)
quantity_delay.get_name()


# In[ ]:



