#!/usr/bin/env python



import os
import sys
import numpy


from Quantity import Quantity


# PARAMETERS:
#   taper
#   defocus
#   code
#   length
#   period
#   seed
#   interpolation

# LIMITS


# VARIABLES:
#   delay
#   phase
#   amplitude
#   doppler


# DERIVED
#   dutycycle
#   samplerate










class Pulse(object):

    def __init__(self,
        #code,
        #length,
        #period,
        #symbolrate,
        #seed=0,
        #taper=1.0,
        #defocus=0.0,
        amplitude,
    ):

        #self.__init__code(code)
        #self.__init__seed(seed)
        #self.__init__amplitude_max(taper)
        #self.__init__defocus(defocus)
        #self.__init__symbolrate(symbolrate)

        #self.__init__length(code,length)
        #self.__init__period(length,period)
        #self.__init__dutycycle(length,period)
        #self.__init__samplerate(symbolrate,interpolation)

        #self.set_delay(delay=0)
        #self.set_phase(phase=0.0)
        #self.set_doppler(doppler=0.0)
        #self.set_amplitude(amplitude=0.0)


        self.__q_amplitude = Quantity(name='Amplitude',unit="a.u.",lower=0.0,upper=3.5,dtype=float,itype='closed')#,value,threshold=)
        self.__q_phase     = Quantity(name='Phase',unit='deg',lower=-180.0,upper=+180.0,dtype=float,itype='closed')
        self.__q_delay     = Quantity(name='Delay',unit='')






        #self.amplitude = self.__q_amplitude.verify(amplitude)


    #def __init__amplitude_max(self,amplitude_max): pass


pulse = Pulse(amplitude=1)


class Quantity(object):

    def __init__(self): pass

    def get_threshold(self): pass
    def set_threshold(self): pass


    def calibrate(self): pass